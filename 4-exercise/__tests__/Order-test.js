import React from 'react';

import '@testing-library/jest-dom/extend-expect';
import '@testing-library/react/cleanup-after-each';

import { render, fireEvent } from '@testing-library/react';

import axios from 'axios';
import 'babel-polyfill';
import Order from '../Order';

jest.mock('axios'); // Mock axios模块

test('Order组件显示异步调用订单数据', async () => {
  // <--start
  // TODO 4: 给出正确的测试
  // setup组件
  const { getByText, getByTestId, getByLabelText } = render(<Order />);
  const button = getByText('订单号');
  const input = getByLabelText('number-input');

  const p = getByTestId('status');
  // Mock数据请
  axios.get.mockResolvedValueOnce({
    data: { status: '已完成' }
  });
  fireEvent.change(input, { target: { value: '12345' } });

  // 触发事件
  await fireEvent.click(button);

  expect(axios.get).toHaveBeenCalledTimes(1);
  expect(axios.get).toHaveBeenLastCalledWith(`/order/${  input.value}`);
  // 给出断言
  expect(p).toHaveTextContent('已完成');
  // --end->
});
